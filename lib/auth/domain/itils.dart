import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:sec_1_new/core/widgets/dialogs.dart';


Future<void> checkAndComplete(BuildContext context, Function onGood) async {
  var connectivityResult = await Connectivity().checkConnectivity();
  if (connectivityResult == ConnectivityResult.none){
    showError(context, "Internet connection failed");
  }else{
    onGood();
  }
}