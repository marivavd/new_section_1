import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

import 'package:sec_1_new/core/run.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> sign_in(
    {required String email,
      required String password,
      required Function(String) onError,
      required Function() onResponse
    })async {
  try {
    final AuthResponse res = await supabase.auth.signInWithPassword(
      email: email,
      password: password,
    );
    onResponse();
  }
  on AuthException catch (e) {
    onError(e.message);
  }
  on Exception catch (e) {
    onError("Something went wrong");
  }
}

Future<void> sign_up(
    {required String email,
      required String password,
      required Function(String) onError,
      required Function() onResponse
    })async {
  try {
    final AuthResponse res = await supabase.auth.signUp(
      email: email,
      password: password,
    );
    onResponse();
  }
  on AuthException catch (e) {
    onError(e.message);
  }
  on Exception catch (e) {
    onError("Something went wrong");
  }
}
Future<void> sign_out(
    {
      required Function(String) onError,
      required Function() onResponse
    })async {
  try {
    await supabase.auth.signOut();
    onResponse();
  }
  on AuthException catch (e) {
    onError(e.message);
  }
  on Exception catch (e) {
    onError("Something went wrong");
  }
}
Future<void> send_email(
    {required String email,
      required Function(String) onError,
      required Function() onResponse
    })async{
  try{
    await supabase.auth.resetPasswordForEmail(email);
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}

Future<void> verify_otp(
    {required String email,
      required String code,
      required Function(String) onError,
      required Function() onResponse
    })async{
  try{
    final AuthResponse res = await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email,
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}


Future<void> change_password(
    {required String password,
      required Function(String) onError,
      required Function() onResponse
    })async{
  try{
    final UserResponse res = await supabase.auth.updateUser(
      UserAttributes(
        password: password,
      ),
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}