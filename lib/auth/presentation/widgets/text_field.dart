import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Custom_Field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool is_obscure;
  final Function()? tap_suffix;
  final MaskTextInputFormatter? formatter;

  const Custom_Field({super.key, required this.label, required this.hint, required this.controller, this.is_obscure = false, this.tap_suffix, this.formatter});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.labelMedium,
        ),
        SizedBox(height: 8,),
        TextField(
          controller: controller,
          inputFormatters: (formatter != null)? [formatter!]: [],
          obscuringCharacter: '*',
          obscureText: is_obscure,
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: Theme.of(context).textTheme.labelMedium!.copyWith(color: Color(0xFFCFCFCF)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                width: 1,
                color: Color(0xFF7576D6)
              )
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
            suffixIcon: (tap_suffix != null) ?
                GestureDetector(
                  child: Image.asset("assets/eye-slash.png"),
                  onTap: tap_suffix,
                ): null
          ),
        )
      ],
    );
  }
}