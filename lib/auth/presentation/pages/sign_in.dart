import 'package:flutter/material.dart';
import 'package:sec_1_new/auth/data/repository/supabase.dart';
import 'package:sec_1_new/auth/domain/itils.dart';

import 'package:sec_1_new/auth/presentation/pages/forgot_pass.dart';
import 'package:sec_1_new/auth/presentation/pages/sign_up.dart';
import 'package:sec_1_new/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new/core/controllers/password_controller.dart';
import 'package:sec_1_new/core/widgets/dialogs.dart';
import 'package:sec_1_new/home/presentation/pages/holder.dart';

class Sign_in_Page extends StatefulWidget {
  const Sign_in_Page({super.key});

  @override
  State<Sign_in_Page> createState() => _Sign_in_PageState();
}

class _Sign_in_PageState extends State<Sign_in_Page> {
  var email_controller = TextEditingController();
  var password_controller = PasswordTextController();
  bool password_obscure = true;
  bool check = false;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Добро пожаловать",
                  style: Theme.of(context).textTheme.labelLarge,
                ),
                SizedBox(height: 8,),
                Text(
                  "Заполните почту и пароль чтобы продолжить",
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                SizedBox(height: 28,),
                Custom_Field(label: "Почта", hint: "***********@mail.com", controller: email_controller),
                SizedBox(height: 24,),
                Custom_Field(label: "Пароль", hint: "**********", controller: password_controller, is_obscure: password_obscure, tap_suffix: (){setState(() {
                  password_obscure = !password_obscure;
                });},),
                SizedBox(height: 18,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(width: 1,),
                        SizedBox(
                          height: 22,
                          width: 22,
                          child: Checkbox(
                              value: check,
                              side: BorderSide(color: Color(0xFF5C636A), width: 1),
                              activeColor: Color(0xFF7576D6),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              onChanged: (bool? val){
                                setState(() {
                                  check = val!;
                                });
                              }),
                        ),
                        SizedBox(width: 8,),
                        Text(
                          "Запомнить меня",
                          style: Theme.of(context).textTheme.labelMedium,
                        )

                      ],
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_pass()));
                      },
                      child: Text(
                        "Забыли пароль?",
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 371,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      style: Theme.of(context).filledButtonTheme.style,
                      onPressed: ()async{
                        checkAndComplete(context, (){
                            sign_in(
                                email: email_controller.text,
                                password: password_controller.getHashText(),
                                onError: (String e){showError(context, e);},
                                onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                                });
                        });


                      },
                      child: Text(
                        'Войти',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w700
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "У меня нет аккаунта! ",
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                      },
                      child: Text(
                        "Создать",
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 32,)
              ],
            ),
          ),
        )
    );
  }
}
