import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:sec_1_new/auth/data/repository/supabase.dart';
import 'package:sec_1_new/auth/domain/itils.dart';
import 'package:sec_1_new/auth/presentation/pages/change_password.dart';
import 'package:sec_1_new/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new/core/controllers/password_controller.dart';
import 'package:sec_1_new/core/widgets/dialogs.dart';

class OTP extends StatefulWidget {
  String email;
  OTP({super.key, required this.email});


  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  var is_error = false;
  var controller = TextEditingController();


  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double separator = widthScreen / 6 - 32;


    return Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Верификация",
                  style: Theme.of(context).textTheme.labelLarge,
                ),
                SizedBox(height: 8,),
                Text(
                  "Введите 6-ти значный код из письма ",
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                SizedBox(height: 28,),
                Pinput(
                  controller: controller,
                  separatorBuilder: (context) => SizedBox(width: separator,),
                  length: 6,
                  defaultPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFFA7A7A7))
                      )
                  ),
                  focusedPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFF0560FA))
                      )
                  ),
                  submittedPinTheme: (!is_error) ? PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFF0560FA)),
                          borderRadius: BorderRadius.zero
                      )
                  ): PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFFED3A3A)),
                          borderRadius: BorderRadius.zero
                      )
                  ),
                  onChanged: (text){
                    setState(() {
                      is_error = false;
                    });
                  },

                ),
                SizedBox(height: 48,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: ()async{
                        await send_email(
                        email: widget.email,
                        onResponse: (){
                        },
                        onError: (String e){showError(context, e);});

                      },
                      child: Text(
                        "Получить новый код",
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(color: Color(0xFF7576D6)),
                      ),
                    )
                  ],
                ),

                SizedBox(height: 503,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      style: Theme.of(context).filledButtonTheme.style,
                      onPressed: ()async{
                        checkAndComplete(context, (){
                          verify_otp(email: widget.email,
                              code: controller.text,
                              onError: (String e){showError(context, e);},
                              onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Change_pass()));
                          });
                        });


                      },
                      child: Text(
                        'Отправить код',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w700
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Я вспомнил свой пароль! ",
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                      },
                      child: Text(
                        "Вернуться",
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 32,)
              ],
            ),
          ),
        )
    );
  }
}
