import 'package:flutter/material.dart';
import 'package:sec_1_new/auth/data/repository/supabase.dart';
import 'package:sec_1_new/auth/domain/itils.dart';

import 'package:sec_1_new/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new/core/controllers/password_controller.dart';
import 'package:sec_1_new/core/widgets/dialogs.dart';

class Sign_up_Page extends StatefulWidget {
  const Sign_up_Page({super.key});

  @override
  State<Sign_up_Page> createState() => _Sign_up_PageState();
}

class _Sign_up_PageState extends State<Sign_up_Page> {
  var email_controller = TextEditingController();
  var password_controller = PasswordTextController();
  var confirm_password_controller = PasswordTextController();
  bool password_obscure = true;
  bool confirm_password_obscure = true;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Создать аккаунт",
                style: Theme.of(context).textTheme.labelLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Завершите регистрацию чтобы начать",
                style: Theme.of(context).textTheme.labelMedium,
              ),
              SizedBox(height: 28,),
              Custom_Field(label: "Почта", hint: "***********@mail.com", controller: email_controller),
              SizedBox(height: 24,),
              Custom_Field(label: "Пароль", hint: "**********", controller: password_controller, is_obscure: password_obscure, tap_suffix: (){setState(() {
                password_obscure = !password_obscure;
              });},),
              SizedBox(height: 24,),
              Custom_Field(label: "Повторите пароль", hint: "**********", controller: confirm_password_controller, is_obscure: confirm_password_obscure, tap_suffix: (){
                setState(() {
                  confirm_password_obscure = !confirm_password_obscure;
                });
              },),
              SizedBox(height: 319,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: ()async{
                      checkAndComplete(context, (){
                        if (password_controller.getHashText() == confirm_password_controller.getHashText())
                        {
                          sign_up(
                              email: email_controller.text,
                              password: password_controller.getHashText(),
                              onError: (String e){showError(context, e);},
                              onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                              });
                        }
                        else{
                          showError(context, "Passwords do not match");
                        }
                      });


                    },
                    child: Text(
                      'Зарегистрироваться',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "У меня уже есть аккаунт! ",
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Войти",
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 32,)
            ],
          ),
        ),
      )
    );
  }
}
