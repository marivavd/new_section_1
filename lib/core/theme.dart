import 'package:flutter/material.dart';

var lightTheme = ThemeData(
  textTheme: TextTheme(
    labelLarge: TextStyle(
      fontSize: 24,
      color: Color(0xFF3A3A3A),
      fontWeight: FontWeight.w500,
    ),
    labelMedium: TextStyle(
      color: Color(0xFF5C636A),
      fontSize: 14,
      fontWeight: FontWeight.w500,
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: Color(0xFF7576D6),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  )
);
var darkTheme = ThemeData(
    textTheme: TextTheme(
        labelLarge: TextStyle(
          fontSize: 24,
          color: Color(0xFF3A3A3A),
          fontWeight: FontWeight.w500,
        ),
        labelMedium: TextStyle(
          color: Color(0xFF5C636A),
          fontSize: 14,
          fontWeight: FontWeight.w500,
        )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            backgroundColor: Color(0xFF7576D6),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            )
        )
    )
);