import 'package:flutter/material.dart';
import 'package:sec_1_new/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new/core/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      darkTheme: darkTheme,
      home: Sign_in_Page(),
    );
  }
}