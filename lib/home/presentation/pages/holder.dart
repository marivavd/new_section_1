import 'package:flutter/material.dart';
import 'package:sec_1_new/auth/data/repository/supabase.dart';
import 'package:sec_1_new/auth/domain/itils.dart';
import 'package:sec_1_new/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new/core/widgets/dialogs.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 46,
          width: 342,
          child: FilledButton(
            style: Theme.of(context).filledButtonTheme.style,
            onPressed: ()async{
              checkAndComplete(context, (){
                sign_out(
                    onError: (String e){showError(context, e);},
                    onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                    });
              });


            },
            child: Text(
              'выход',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w700
              ),
            ),
          ),
        ),
      ),
    );
  }
}
